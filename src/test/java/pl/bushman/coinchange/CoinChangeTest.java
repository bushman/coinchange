package pl.bushman.coinchange;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CoinChangeTest {
    private final CoinChange coinChange = new CoinChange();

    @Test
    public void countsWays() {
        Integer[] coins = {1, 5, 2, 10};
        int resultWays = coinChange.waysCount(8, coins);

        assertEquals(7, resultWays);//{5,2,1} {5,1,1,1} {2,2,2,2} {2,2,2,1,1} {2,2,1,1,1,1} {2,1,1,1,1,1,1} {1,1,1,1,1,1,1,1,1}
    }

    @Test
    public void returnsZeroWaysWhenAmountZero() {
        Integer[] coins = {1, 5, 2, 10};
        int resultWays = coinChange.waysCount(0, coins);

        assertEquals(0, resultWays);
    }

    @Test
    public void returnsZeroWaysWhenUnableToChange() {
        Integer[] coins = {10, 5};
        int resultWays = coinChange.waysCount(11, coins);

        assertEquals(0, resultWays);
    }


}
