package pl.bushman.coinchange;

import java.util.Arrays;
import java.util.Comparator;

public class CoinChange {

    public int waysCount(int amount, Integer[] coins) {
        Arrays.sort(coins, Comparator.comparingInt(o -> -o));

        return waysInternal(amount, coins, 0);
    }

    private int waysInternal(int amount, Integer[] coins, int coinsStartingIndex) {
        int sum = 0;
        for (int i = coinsStartingIndex; i < coins.length; i++) {
            if (coins[i] == amount) {
                sum++;
            } else if (coins[i] < amount) {
                sum += waysInternal(amount - coins[i], coins, i);
            }
        }
        return sum;
    }
}
